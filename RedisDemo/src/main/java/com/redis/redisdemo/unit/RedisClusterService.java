package com.redis.redisdemo.unit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import java.util.concurrent.TimeUnit;

@Service
public class RedisClusterService {
    /**
     * 使用StringRedisTemplate模板
     */
    @Autowired
    private StringRedisTemplate stringRedisTemplate;



    /**
     * 设置键值
     * @param key 键名
     * @param value 键值
     */
    public void setStr(String key, String value) {
        setStr(key, value, null);
    }

    /**
     * 设置键值
     * @param key 键名
     * @param value 键值
     * @param time 缓存失效时间
     */
    public void setStr(String key, String value, Long time) {
        //使用String模板
        stringRedisTemplate.opsForValue().set(key,value);
        if(time != null){
            stringRedisTemplate.expire(key, time, TimeUnit.SECONDS);
        }
    }

    /**
     * 获取键值
     * @param key 键名
     * @return value 键值
     */
    public Object getKey(String key) {
        return stringRedisTemplate.opsForValue().get(key);
    }

    /**
     * 删除键
     * @param key 键名
     */
    public void delKey(String key) {
        stringRedisTemplate.delete(key);
    }
}
