package com.redis.redisdemo.controller;

import com.redis.redisdemo.unit.RedisClusterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HomeController {
    /**
     * 将Redis集群工具类注入
     */
    @Autowired
    private RedisClusterService redisClusterService;

    /**
     * 调用set方法赋值
     */
    @CrossOrigin
    @RequestMapping("/setRedis")
    public Object setRedisValue(String key,String value){
        redisClusterService.setStr(key,value);
        return "success";
    }

    /**
     * 调用get方法获取值
     */
    @CrossOrigin
    @RequestMapping("/getKey")
    public Object getKey(String key){
        Object result = redisClusterService.getKey(key);
        return result == null?"缓存中没有该数据":result;
    }
}