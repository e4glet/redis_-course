package com.redis.redisdemo;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;

@SpringBootTest
class RedisDemoApplicationTests {

    /**
     * 自动装配RedisTemplate对象
     */
    @Autowired
    private RedisTemplate redisTemplate;

    /**
     * 首先我们知道Redis是Key-Value存储结构的数据库
     * 因此我们假定用set()方法向Redis数据库中存储数据
     */
    @Test
    void testSet(){
        //新建Key-Value操作对象，并使用redisTemplate声明当前的数据是以key-value形式
        ValueOperations<String,String> ops = redisTemplate.opsForValue();
        //然后调用set方法进行赋值
        ops.set("myproduct","cocol");
    }

    /**
     * 从Redis数据库中取出刚刚设置的key-value的值
     */
    @Test
    void testGet(){
        //新建Key-Value操作对象，并使用redisTemplate声明当前的数据是以key-value形式
        ValueOperations<String,String> ops = redisTemplate.opsForValue();
        //调用get方法取出key对应的值
        Object obj = ops.get("myproduct");
        System.out.println(obj);
    }
}
