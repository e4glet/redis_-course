# Redis 快速入门与提高教程

### 介绍
Redis是一个key-value存储系统,具备内存级性能的NoSQL数据库。  
其主要应用在分布式缓存、令牌认证等功能。  
redis提供五种数据类型：string，hash，list，set及zset(sorted set)。  


### 部署redis服务器

#### 单机模式  
下载地址：https://github.com/tporadowski/redis/releases
![输入图片说明](imgs/redis01.png)  
解压后，运行目录中的redis-server.exe，启动Redis。  
![输入图片说明](imgs/redis02.png)

#### 集群模式  
使用微服务部署服务器集群是非常方便的，因此这里我们使用docker进行部署。

打开docker hub官网查询Redis版本：https://hub.docker.com/  
这里我们选择7.0.11版本  

1. 使用docker拉取镜像：  
```
docker pull redis:7.0.11
```

2. 配置redis集群文件 
创建redis-cluster目录，并在目录下创建文件redis-cluster.tmpl  
命令：
```
mkdir redis-cluster
cd redis-cluster
vi redis-cluster.tmpl
```
集群配置文件redis-cluster.tmpl内容如下：  
```
port ${PORT}    
cluster-enabled yes   
cluster-config-file nodes.conf  
cluster-node-timeout 5000  
cluster-announce-ip 192.168.237.129 
cluster-announce-port ${PORT}
cluster-announce-bus-port 1${PORT}
appendonly yes 
protected-mode no
requirepass 123456
```

命令解释：  
```
port ${PORT}    #端口7000,7002,7003  
#bind 本机ip     #这里我们通过打开每个节点7000-7005端口，设置cluster-announce-ip和端口来进行集群创建  
cluster-enabled yes    #开启集群  
cluster-config-file nodes.conf  #集群节点的配置  
cluster-node-timeout 5000  #请求超时  
cluster-announce-ip 192.168.237.129 #自己服务器IP  
cluster-announce-port ${PORT}   #通知端口  
cluster-announce-bus-port 1${PORT}   #集群总线端口  
appendonly yes  #开启持久化 
protected-mode no #关闭保护模式 
requirepass 123456  #设置密码
```

3. 创建配置文件夹及文件  
```
for port in `seq 7000 7005`; do \
  mkdir -p ./${port}/conf \
  && PORT=${port} envsubst < ./redis-cluster.tmpl > ./${port}/conf/redis.conf \
  && mkdir -p ./${port}/data; \
done
```
![输入图片说明](imgs/redis10.png)  


4. 创建docker自定义网络
```
docker network create --subnet=182.18.0.0/16 --gateway=182.18.0.1 data-net
```

5. 创建对应端口的六个节点容器
注意：这里使用shell循环创建容器  
我们创建的自定义网络里如果地址和网关也是自定义  
docker 创建容器的时候也必须指定ip  
否则默认创建方法 容器获取不到ip而无法启动  
```
for port in `seq 0 5`; do \
  docker run --privileged=true --ip 182.18.0.2${port} -d -ti -p 700${port}:700${port} -p 1700${port}:1700${port} \
  -v /root/redis-cluster/700${port}/conf/redis.conf:/usr/local/etc/redis/redis.conf \
  -v /root/redis-cluster/700${port}/data:/data \
  --restart always --name redis-700${port} --net data-net \
  --sysctl net.core.somaxconn=1024 redis:7.0.11 redis-server /usr/local/etc/redis/redis.conf; \
done
```
![输入图片说明](imgs/redis11.png) 


6. 进入redis服务器首节点并创建redis集群关系  
进入redis-7000  
```
docker exec -it redis-7000 /bin/bash
```
建立集群关系  
```
redis-cli -a 123456 --cluster create 192.168.237.129:7000 192.168.237.129:7001 192.168.237.129:7002 192.168.237.129:7003 192.168.237.129:7004 192.168.237.129:7005 --cluster-replicas 1
```
在提示下输入yes （注意是yes，是yes，是yes，不能只写一个字母y）  

7. 查看redis集群运行状态

进入主节点查看集群
```
redis-cli -h 192.168.237.129 -p 7000 -a 123456
```
查看集群
```
cluster info
```
![输入图片说明](imgs/redis12.png)


8. 删除集群命令
```
for port in `seq 7000 7005`; do \
 docker stop redis-${port}; \
 docker rm redis-${port}; \
done
```

9. docker缓存的清除命令
```
docker builder prune
```

### SpringBoot整合redis

1. 创建Springboot并添加依赖  
![输入图片说明](imgs/redis03.png)  
![输入图片说明](imgs/redis04.png)  

pom.xml依赖项： 
```
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-data-redis</artifactId>
        </dependency>
```

2. application.yml配置信息  
Redis单机配置方式：
```
spring:
  redis:
    host: localhost # 127.0.0.1
    port: 6379
    # password: 123456 # 如果没有配置这里就不写
```
Redis集群配置方式：
```
spring:
  # redis 集群配置
  redis:
    password: 123456
    cluster: # 配置集群
      nodes: # 配置节点
        # 集群节点及对应服务器的端口
        - 192.168.237.129:7000
        - 192.168.237.129:7001
        - 192.168.237.129:7002
        - 192.168.237.129:7003
        - 192.168.237.129:7004
        - 192.168.237.129:7005
    jedis:
      pool:
        enabled: on # 启动连接池
        max-idle: 10 # 最大连接池空闲链接
        max-active: 1000 # 连接池最大连接数
        max-wait: -1 # 连接池最大阻塞等待时间，负数表示没有限制
      database : 0 # Redis数据库索引（默认为0）
      timeout : 5000 # 超时时间 ms
```

3. 添加功能代码
在测试类中对Redis数据库进行读写操作
```
package com.redis.redisdemo;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;

@SpringBootTest
class RedisDemoApplicationTests {

    /**
     * 自动装配RedisTemplate对象
     */
    @Autowired
    private RedisTemplate redisTemplate;

    /**
     * 首先我们知道Redis是Key-Value存储结构的数据库
     * 因此我们假定用set()方法向Redis数据库中存储数据
     */
    @Test
    void testSet(){
        //新建Key-Value操作对象，并使用redisTemplate声明当前的数据是以key-value形式
        ValueOperations ops = redisTemplate.opsForValue();
        //然后调用set方法进行赋值
        ops.set("mykey01","我存储的值");
    }

    /**
     * 从Redis数据库中取出刚刚设置的key-value的值
     */
    @Test
    void testGet(){
        //新建Key-Value操作对象，并使用redisTemplate声明当前的数据是以key-value形式
        ValueOperations ops = redisTemplate.opsForValue();
        //调用get方法取出key对应的值
        Object obj = ops.get("mykey01");
        System.out.println(obj);
    }
}

```
分别运行testSet和testGet方法：  
![输入图片说明](imgs/redis05.png)  
![输入图片说明](imgs/redis06.png)  
此时我们已经看到存储到Redis数据库的值已经取出。

4. 实际应用方式
提示：这里修改了RedisTamplate模板的类型，这样在redis数据客户端中才能访问到我们的数据对象。

改进修改我们的代码：  
```
package com.redis.redisdemo;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.ValueOperations;

@SpringBootTest
class RedisDemoApplicationTests {

    /**
     * 自动装配RedisTemplate对象
     */
    @Autowired
    private StringRedisTemplate stingRedisTemplate;

    /**
     * 首先我们知道Redis是Key-Value存储结构的数据库
     * 因此我们假定用set()方法向Redis数据库中存储数据
     */
    @Test
    void testSet(){
        //新建Key-Value操作对象，并使用redisTemplate声明当前的数据是以key-value形式
        ValueOperations<String,String> ops = stingRedisTemplate.opsForValue();
        //然后调用set方法进行赋值
        ops.set("mykey01","Hello redis.");
    }

    /**
     * 从Redis数据库中取出刚刚设置的key-value的值
     */
    @Test
    void testGet(){
        //新建Key-Value操作对象，并使用redisTemplate声明当前的数据是以key-value形式
        ValueOperations<String,String> ops = stingRedisTemplate.opsForValue();
        //调用get方法取出key对应的值
        Object obj = ops.get("mykey01");
        System.out.println(obj);
    }
}

```
这时，双击打开我们的Redis数据库目录中的客户端程序redis-cli.exe。  
![输入图片说明](imgs/redis07.png)

仅仅到这里就完了吗？当然不是，这里我们只是了解和掌握对Redis数据库的基本操作。  
在真实项目中使用Redis，我们还有很多内容要掌握。

### Redis在真实项目中的作用

1. 高速缓存
2. token令牌

### 实际项目应用

1.  新建Redis集群工具类
```
package com.redis.redisdemo.unit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import java.util.concurrent.TimeUnit;

@Service
public class RedisClusterService {
    /**
     * 使用StringRedisTemplate模板
     */
    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    /**
     * 设置键值
     * @param key 键名
     * @param value 键值
     */
    public void setStr(String key, String value) {
        setStr(key, value, null);
    }

    /**
     * 设置键值
     * @param key 键名
     * @param value 键值
     * @param time 缓存失效时间
     */
    public void setStr(String key, String value, Long time) {
        //使用String模板
        stringRedisTemplate.opsForValue().set(key,value);
        if(time != null){
            stringRedisTemplate.expire(key, time, TimeUnit.SECONDS);
        }
    }

    /**
     * 获取键值
     * @param key 键名
     * @return value 键值
     */
    public Object getKey(String key) {
        return stringRedisTemplate.opsForValue().get(key);
    }

    /**
     * 删除键
     * @param key 键名
     */
    public void delKey(String key) {
        stringRedisTemplate.delete(key);
    }
}

```
2.  在项目中增加SpringMVC依赖

```
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-web</artifactId>
        </dependency>
```
3.  在新建控制器类并添加测试代码
```
package com.redis.redisdemo.controller;

import com.redis.redisdemo.unit.RedisClusterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HomeController {
    /**
     * 将Redis集群工具类注入
     */
    @Autowired
    private RedisClusterService redisClusterService;

    /**
     * 调用set方法赋值
     */
    @CrossOrigin
    @RequestMapping("/setRedis")
    public Object setRedisValue(String key,String value){
        redisClusterService.setStr(key,value);
        return "success";
    }

    /**
     * 调用get方法获取值
     */
    @CrossOrigin
    @RequestMapping("/getKey")
    public Object getKey(String key){
        Object result = redisClusterService.getKey(key);
        return result == null?"缓存中没有该数据":result;
    }
}

```
启动SpringBoot项目，在浏览器中输入  
赋值地址：http://localhost:8080/setRedis?key=wujing&value=zhanlang  
![输入图片说明](imgs/redis08.png)  
取值地址：http://localhost:8080/getKey?key=wujing  
![输入图片说明](imgs/redis09.png)


4.接口测试好后，就可以根据需要使用统一的Response对象，将缓存或者验证的Token数据存储到我们的Redis集群中。 


5.获取更加完善的redis工具类  
参考该篇文章：https://blog.csdn.net/qq_18841277/article/details/126728124

### 演示项目
将演示项目RedisDemo使用IDEA导入即可。

### 安全提示
在生产环境中，一定要给Redis数据服务添加密码。这个千万不要忘记！！！

### 作者

1.  e4glet
